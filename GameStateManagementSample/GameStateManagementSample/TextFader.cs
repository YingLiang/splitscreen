using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;


namespace GameStateManagementSample
{
    /// <summary>
    /// This is a game component that implements IUpdateable.
    /// </summary>
    public class TextFader : Microsoft.Xna.Framework.DrawableGameComponent
    {
        enum State { Visible, Inactive };
        State state = State.Inactive;
        String text = "";
        float timeVisible;
        float currentTime;
        SpriteBatch spriteBatch;
        SpriteFont font;
        public TextFader(Game game)
            : base(game)
        {
            game.Services.AddService(typeof(TextFader), this);
            // TODO: Construct any child components here
        }
        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(Game.GraphicsDevice);
            font = Game.Content.Load<SpriteFont>("menufont");
            base.LoadContent();
        }

        /// <summary>
        /// Allows the game component to perform any initialization it needs to before starting
        /// to run.  This is where it can query for any required services and load content.
        /// </summary>
        public override void Initialize()
        {
            // TODO: Add your initialization code here

            base.Initialize();
        }

        /// <summary>
        /// Allows the game component to update itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Update(GameTime gameTime)
        {
            // TODO: Add your update code here
            if (state == State.Inactive) return;
            currentTime += (float)gameTime.ElapsedGameTime.TotalSeconds;
            if (currentTime >= timeVisible) { state = State.Inactive; }
            base.Update(gameTime);
        }
        public override void Draw(GameTime gameTime)
        {
            if (state == State.Inactive) return;
            spriteBatch.Begin();
            spriteBatch.DrawString(font, text, new Vector2(50, 50), Color.White);
            spriteBatch.End();
            base.Draw(gameTime);
        }
        public void Show(String txt, float duration) 
        {
            text = txt;
            timeVisible = duration;
            state = State.Visible;
            currentTime = 0;
        }
    }
}
